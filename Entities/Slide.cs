﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashCode.Entities
{
    public class Slide
    {
        public List<Photo> Photos { get; }
        public HashSet<string> Tags { get; }

        public Slide(params Photo[] photos)
        {
            Photos = new List<Photo>(photos);
            Tags = new HashSet<string>();
            foreach (var photo in photos)
            {
                foreach (var tag in photo.Tags)
                {
                    Tags.Add(tag);
                }
            }
        }

        public int InterestFactor(Slide nextSlide)
        {
            /* INTERSECT */
            var commonTags = 0;
            foreach (var tag in Tags)
            {
                /* IN COMMON */
                if (nextSlide.Tags.Contains(tag))
                {
                    commonTags++;
                }
            }

            var missingTagsFromSecondSlide = Tags.Count - commonTags;
            var missingTagsFromFirstSlide = nextSlide.Tags.Count - commonTags;
            ///* TAG MISSING FROM FIRST SLIDE */
            //var missingTagsFromFirstSlide = nextSlide.Tags.Count;
            //foreach (var tag in nextSlide.Tags)
            //{
            //    if (Tags.Contains(tag))
            //        missingTagsFromFirstSlide--;
            //}

            return Math.Min(commonTags, Math.Min(missingTagsFromSecondSlide, missingTagsFromFirstSlide));
        }

        public override string ToString()
        {
            if(Photos.Count == 1)
                return Photos[0].Id.ToString();
            else
                return $"{Photos[0].Id.ToString()} {Photos.Last().Id.ToString()}";
        }
    }
}
