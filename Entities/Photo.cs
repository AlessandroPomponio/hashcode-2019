﻿using System.Collections.Generic;
using System.Text;

namespace HashCode.Entities
{
    public class Photo
    {
        public static ushort HORIZONTAL = 0;
        public static ushort VERTICAL = 1;

        public int Id { get; set; }
        public ushort Orientation { get; set; }
        public int AmountOfTags { get; set; }
        public ICollection<string> Tags { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder($"Photo with ID {Id} has orientation {Orientation} and has {AmountOfTags} tags:\n");
            foreach(var tag in Tags)
            {
                sb.Append(tag).Append(", ");
            }
            return sb.ToString();
        }
    }
}
