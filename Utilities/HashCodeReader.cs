﻿using HashCode.Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace HashCode.Utilities
{
    public static class HashCodeReader
    {
        public static IList<Photo> ReadFile(string filename)
        {
            try
            {
                using (StreamReader sr = new StreamReader(filename))
                {
                    /* AMOUNT OF PHOTOS IN THE COLLECTION */
                    int lines = int.Parse(sr.ReadLine());

                    /* READ ALL PHOTOS */
                    IList<Photo> photos = new List<Photo>();
                    for(var i = 0; i < lines; i++)
                    {
                        /* CURRENT PHOTO */
                        Photo photo = new Photo
                        {
                            Id = i
                        };

                        /* READ THE CURRENT LINE */
                        string line = sr.ReadLine();

                        /* SPLIT */
                        var fields = line.Split(' ');

                        /* PARSE ORIENTATION */
                        if(fields[0] == "H")
                            photo.Orientation = Photo.HORIZONTAL;
                        else
                            photo.Orientation = Photo.VERTICAL;

                        /* AMOUNT OF TAGS */
                        photo.AmountOfTags = int.Parse(fields[1]);

                        /* ADD ALL TAGS */
                        HashSet<string> tags = new HashSet<string>();
                        for(var j = 0; j < photo.AmountOfTags; j++)
                        {
                            tags.Add(fields[2 + j]);
                        }

                        photo.Tags = tags;
                        photos.Add(photo);
                    }

                    return photos;
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            return null;
        }
    }
}
