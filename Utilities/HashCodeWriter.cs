﻿using HashCode.Entities;
using System.Collections.Generic;
using System.IO;

namespace HashCode.Utilities
{
    public static class HashCodeWriter
    {
        public static void WriteToFile(string filename, ICollection<Slide> slides)
        {
            using (StreamWriter file = new StreamWriter(filename))
            {
                file.WriteLine(slides.Count);

                foreach(var slide in slides)
                {
                    file.WriteLine(slide);
                }
            }
        }
    }
}
