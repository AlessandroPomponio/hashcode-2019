﻿using HashCode.Entities;
using HashCode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashCode
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileName = "a_example.txt";
            var result = HashCodeReader.ReadFile(fileName);

            /* DUE FOTO */
            var firstPhoto = result.First();
            var lastPhoto = result.Last();
            
            /* TRE SLIDES */
            var firstSlide = new Slide(firstPhoto, lastPhoto);
            var secondSlide = new Slide(firstPhoto);
            var thirdSlide = new Slide(lastPhoto);

            var intFact1 = firstSlide.InterestFactor(secondSlide);




            //var slides = new List<Slide>();
            //slides.Add(new Slide(firstPhoto, lastPhoto));
            //slides.Add(new Slide(firstPhoto));
            //slides.Add(new Slide(lastPhoto));
            //HashCodeWriter.WriteToFile(fileName + "res.txt", slides);
            Console.WriteLine("Press a key to exit");
            Console.ReadKey();
        }
    }
}
