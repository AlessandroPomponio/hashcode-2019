﻿using HashCode.Entities;
using HashCode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HashCode
{
    internal static class DomeSolution
    {
        private static readonly List<List<Slide>> ResultSlides = new List<List<Slide>>();

        private static void Main()
        {
            const string a_example = "a_example.txt";
            const string b_lovely_landscapes = "b_lovely_landscapes.txt";
            const string c_memorable_moments = "c_memorable_moments.txt";
            const string d_pet_pictures = "d_pet_pictures.txt";
            const string e_shiny_selfies = "e_shiny_selfies.txt";
            const int parts = 10;

            CreateSlideshow(a_example, parts);
            CreateSlideshow(b_lovely_landscapes, parts);
            CreateSlideshow(c_memorable_moments, parts);
            CreateSlideshow(d_pet_pictures, parts);
            CreateSlideshow(e_shiny_selfies, parts);
        }

        private static void CreateSlideshow(string fileName, int parts)
        {
            /* SET UP */
            ResultSlides.Clear();

            /* READ PHOTOS FROM FILE */
            var photos = HashCodeReader.ReadFile(fileName);
            Console.WriteLine("Completata lettura da file {0}", fileName);

            /* CREATE SLIDES AND ISOLATE VERTICAL PHOTOS */
            List<Slide> slides = new List<Slide>();
            List<Photo> vPhotos = new List<Photo>();
            foreach (Photo photo in photos)
            {
                if (photo.Orientation == Photo.HORIZONTAL)
                {
                    Slide slide = new Slide(photo);
                    slides.Add(slide);
                }
                else
                {
                    vPhotos.Add(photo);
                }
            }
            Console.WriteLine("Elaborate foto orizzontali");

            /* CREATE SLIDES FOR VERTICAL PHOTOS */
            IList<Slide> vSlides = MergeVertical(vPhotos);
            Console.WriteLine("Elaborate foto verticali");

            /* MERGE HORIZONTAL AND VERTICAL SLIDES */
            slides.AddRange(vSlides);
            slides.OrderByDescending(s => s.Tags.Count);
            Console.WriteLine("Slides unite");

            /* SPLIT SLIDES TO USE CONCURRENCY */
            var slideParts = Split(slides, parts);
            slideParts.TrimExcess();
            Console.WriteLine("Slides divise in {0} parti", parts);

            /* CREATE AND RUN TASKS */
            List<Task> tasks = new List<Task>();
            foreach (var part in slideParts)
            {
                tasks.Add(Task.Factory.StartNew(() => CreateSlideshowPart(part)));
            }
            Console.WriteLine("Task avviati");
            Task.WaitAll(tasks.ToArray());

            /* MERGE RESULTS */
            List<Slide> finalResults = new List<Slide>();
            foreach (var resultSlide in ResultSlides)
            {
                finalResults.AddRange(resultSlide);
            }

            /* WRITE RESULTS TO FILE */
            HashCodeWriter.WriteToFile(fileName + "_result.txt", finalResults);
            Console.WriteLine("Scritti risultati su {0}_results.txt\n\n", fileName);
        }

        public static List<Slide> MergeVertical(IList<Photo> verticalPhotos)
        {
            /* ORDER BY THE NUMBER OF TAGS AND COUNT HOW MANY SLIDES WE'LL HAVE */
            verticalPhotos.OrderBy(p => p.Tags.Count);
            int photosAmount = verticalPhotos.Count - 1;
            int slidesAmount = verticalPhotos.Count / 2;

            List<Slide> slides = new List<Slide>();
            for (int i = 0; i < slidesAmount; i++)
            {
                slides.Add(new Slide(verticalPhotos[i], verticalPhotos[photosAmount - i]));
            }

            return slides;
        }

        public static List<List<Slide>> Split(List<Slide> slides, int parts)
        {
            List<List<Slide>> slideParts = new List<List<Slide>>();
            var elements = slides.Count / parts;
            for (var i = 0; i < parts; i++)
            {
                var buffer = new Slide[elements];
                slides.CopyTo(i * elements, buffer, 0, elements);
                slideParts.Add(buffer.ToList());
            }

            return slideParts;
        }

        public static void CreateSlideshowPart(List<Slide> slides)
        {
            /* AVOID EXCEPTIONS OF SORTS */
            if (slides.Count == 0)
                return;

            /* CREATE THE SLIDESHOW BY ADDING THE FIRST SLIDE */
            List<Slide> slideshow = new List<Slide>
            {
                slides[0]
            };
            slides.Remove(slides[0]);

            while (slides.Count > 0)
            {
                int maxInterestFactor = -1;
                Slide currentSlide = null;
                Slide current = slideshow.Last();

                for (int i = 0; i < slides.Count; i++)
                {
                    int currentInterestfactor = current.InterestFactor(slides[i]);
                    if (currentInterestfactor > maxInterestFactor)
                    {
                        currentSlide = slides[i];
                        maxInterestFactor = currentInterestfactor;
                    }
                }

                slideshow.Add(currentSlide);
                slides.Remove(currentSlide);
            }

            ResultSlides.Add(slideshow);
        }
    }
}
